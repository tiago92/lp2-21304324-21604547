package pt.ulusofona.lp2.jungleGame;

import java.io.*;


public class Animal {

    int posicao;
    String nome;
    int id;
    int idEspecie;
    int velocidadeBase;
    int energiaInicial;
    String imagePNG;
    int inteligencia;


    int ultimoTurno;

    public Animal(String nome, int id,int idEspecie, int velocidadeBase, int energiaInicial, String image, int inteligencia) {
        this.nome = nome;
        this.id = id;
        this.idEspecie =idEspecie;
        this.velocidadeBase = velocidadeBase;
        this.energiaInicial = energiaInicial;
        this.imagePNG = image;

    }




    public int getPosicao (){ //Devolve posição actual do Animal.

        return posicao;
    }

    public String getImagePNG(){ //Devolve nome do ficheiro de imagem (PNG), que representa o Animal.
        return imagePNG;
    }

    public void escreveEmFicheiro(int turno, int totalTurnos, String nomeFile){
        try{
            File file = new File(nomeFile);
            if(!file.exists()){
                file.createNewFile();
            }
            FileWriter fileWritter = new FileWriter(file.getName(),true);
            BufferedReader bR = new BufferedReader(new FileReader(nomeFile));
            BufferedWriter bufferWritter = new BufferedWriter(fileWritter);

            if(turno==0)
                bufferWritter.write("CLASSIFICACAO FINAL DA PROVA"+"\r\n");



            bufferWritter.close();
            bR.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public boolean move(){
        int distancia = (int)(velocidadeBase * energiaInicial * 0.5 + 1);// cast de double para int

        if(verificaEnergia()){
            //se energia >0 e o movimento não o fizer sair do mapa ele entra
            if((posicao + distancia) <= 11){
                posicao = posicao + distancia;
                energiaInicial=--energiaInicial;
            }else{
                posicao=11;
            }
        }else{
            energiaInicial+=2;
        }

        if(posicao==11)
            return false;

        ultimoTurno++;

        return true;

    }

    public boolean verificaEnergia(){
        if(energiaInicial>0)
            return true;
        else
            return false;
    }

    public String getNome() {
        return nome;
    }

    public int getUltimoTurno() {
        return ultimoTurno;
    }

}



    
    

