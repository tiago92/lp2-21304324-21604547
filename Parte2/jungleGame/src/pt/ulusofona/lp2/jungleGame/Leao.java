package pt.ulusofona.lp2.jungleGame;

public class Leao extends Animal {
    public Leao(String nome, int id, int idEspecie, int velocidadeBase, int energiaInicial, String image, int inteligencia) {
        super(nome, id, idEspecie, velocidadeBase, energiaInicial, image, inteligencia);
    }
}
