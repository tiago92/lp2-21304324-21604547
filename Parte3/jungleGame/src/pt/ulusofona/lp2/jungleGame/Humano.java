package pt.ulusofona.lp2.jungleGame;

public class Humano extends Animal {

	public Humano(String nome, int id, int velocidadeBase, int energiaInicial, String image,
			int inteligencia) {
		super(nome, id,3, velocidadeBase, energiaInicial, image, inteligencia);
	}
}
