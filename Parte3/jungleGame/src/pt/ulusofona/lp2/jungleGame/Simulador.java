package pt.ulusofona.lp2.jungleGame;

import java.io.*;
import java.util.*;




public class Simulador {

    List <Animal> animais = new ArrayList<>();
    List <Premio> premios = new ArrayList<>();

    List<Terreno> terrenos = new ArrayList<>();
    
    int nPersonagens;
    int nPremios;
    int nFenomenos;

    int numeroFinal;
    int numeroTurno;
    
    List<Fenonomo> fenonomos = new ArrayList<>();
    
    HashMap<Animal, Integer> posicoesFinais = new HashMap<>();
    HashMap<Animal, Integer> temposFinais = new HashMap<>();
    
    public void iniciaJogo (File ficheiroInicial, int comprimentoPista) throws InvalidSimulatorInputException{ //Faz a leitura do ficheiro texto e carrega para a memória os animais e prémios presentes na simulação
        String[] dados;

        try {
            Scanner leitorFicheiro = new Scanner(ficheiroInicial);

            String linha1;
            
            linha1 = leitorFicheiro.nextLine();
            dados = linha1.split(":");
            this.nPersonagens= Integer.parseInt(dados[0]);
            this.nPremios = Integer.parseInt(dados[1]);           
            this.nFenomenos = Integer.parseInt(dados[2]); 
            
            for (int i=0; i<nPersonagens; i++){
                    linha1 = leitorFicheiro.nextLine();
                    dados = linha1.split(":");
                    String nome = dados[0];
                    int id = Integer.parseInt(dados[1]);
                    int idEspecie = Integer.parseInt(dados[2]);
                    int velocidadeBase = Integer.parseInt(dados[3]);
                    int energiaInicial = Integer.parseInt(dados[4]);
                    int inteligencia = Integer.parseInt(dados[5]);

                    switch (idEspecie) {
                    case 1:
                        animais.add(new Formiga(nome,id,velocidadeBase,energiaInicial,"tarzan.png",inteligencia));
                        break;

                    case 2:
                        animais.add(new Girafa(nome,id,velocidadeBase,energiaInicial,"tarzan.png",inteligencia));
                        break;

                    case 3:
                        animais.add(new Humano(nome,id,velocidadeBase,energiaInicial,"tarzan.png",inteligencia));
                        break;

                        case 4:
                            animais.add(new Leao(nome,id,velocidadeBase,energiaInicial, "simba.jpg",inteligencia));
                            break;

                        case 5:
                            animais.add(new Lebre(nome,id,velocidadeBase,energiaInicial,"tarzan.png",inteligencia));
                            break;

                        case 6:
                            animais.add(new Mocho(nome,id,velocidadeBase,energiaInicial, "coruja.jpg", inteligencia));
                            break;
                            
                        case 7:
                            animais.add(new Tartaruga(nome,id,velocidadeBase,energiaInicial,"tarzan.png",inteligencia));
                            break;

                        case 8:
                            animais.add(new Tigre(nome,id,velocidadeBase,energiaInicial,"tarzan.png",inteligencia));
                            break;

                        case 9:
                            animais.add(new Zebra(nome,id,velocidadeBase,energiaInicial,"tarzan.png",inteligencia));
                            break;

                    }
            }

            for (int i=0; i<nPremios;i++){
                linha1 = leitorFicheiro.nextLine();
                dados = linha1.split("\\s*:\\s*");
                String nomePremio = dados[0];
                int valor = Integer.parseInt(dados[1]);
                int posicao = Integer.parseInt(dados[2]);

                premios.add(new Premio(nomePremio,valor,posicao));
            }

            for (int i=0; i<nFenomenos;i++){
                linha1 = leitorFicheiro.nextLine();
                dados = linha1.split("\\s*:\\s*");
                
                int id = Integer.parseInt(dados[0]);
                int turno = Integer.parseInt(dados[1]);

                this.fenonomos.add(new Fenonomo(id, turno));
            }

            linha1 = leitorFicheiro.nextLine();
            for (int i=0; i<linha1.length(); i++) {
            	Terreno t;
            	
            	if (i>=comprimentoPista-1) {
            		break;
            	}
            	
            	switch (linha1.charAt(i)) {
            	case 'A': t = new Agua(); break; 
            	case 'R': t = new Rochoso(); break; 
            	case 'L': t = new Lama(); break; 
            	case 'M': t = new Montanha(); break; 
            	default: t = new Terra(); break; 
            	}
            	
            	terrenos.add(t);
            }
            
            while (terrenos.size()<comprimentoPista-1) {
            	terrenos.add(new Terra());
            }

            leitorFicheiro.close();            
        }catch (FileNotFoundException exception) {
            String mensagem = "Erro nao foi possivel abrir input.txt";
            throw new InvalidSimulatorInputException(mensagem, 0);
        }

    }

    public String getBackgroundImagePNG(int id) {
    	switch (id) {
    	//case 1: return "terreno1.jpg";
    	default:return "grass.jpg";
    	}
    		
    }
    
    public boolean processaTurno(){  //Executa o turno/tempo da simulação

    	numeroTurno++;
    	
        boolean novaChegadaAoFim =false;

        for (Animal animal : animais){ //Percorre os animais e faz a sua distância

        	if (posicoesFinais.containsKey(animal)) {
        		continue;
        	}
        	
        	if (animal.getPosicao()<terrenos.size()) {
	            Terreno t = terrenos.get(animal.getPosicao());
	        	
	            boolean a = animal.move(numeroTurno, t, terrenos.size(), this.fenonomos);
	             if(!a){
	            	 
	            	 if (novaChegadaAoFim == false) {
	            		 novaChegadaAoFim = true;
	            		 this.numeroFinal++;
	            	 }

	            	 posicoesFinais.put(animal, this.numeroFinal);
	            	 temposFinais.put(animal, this.numeroTurno);
	             }
        	}
        }

        if (posicoesFinais.size() == animais.size())
            return false;
        
        return true;
    }


    public List<Premio> getPremios() {
        return premios;
    }

    public List<Premio> getListaPremioPorPosicao(int c, List<Premio> premios)  {
        List<Premio> temp = new ArrayList<>();
        for (Premio p1: premios) {
            if (p1.posicaoPremio == c){
                temp.add(p1);
            }
        }
        return temp;
    }


    public List <Animal> getAnimais (){ //Lista de todos os animais presentes na simulção

        return animais;
    }

    public List<String> getClassificacaoGeral(String s){ //Lista de Strings quem representa a classificação geral da prova
        List<String> lista= new ArrayList<>();


        lista.add("CLASSIFICACAO FINAL DA PROVA \n");

        for (int i=1; i<=animais.size(); i++) {
            for (Animal a: animais) {
            	if (this.posicoesFinais.get(a) == i) {
            		int tempo = temposFinais.get(a);
                    lista.add(a.getNome() + " " + a.getIdEspecie()+ " "+tempo +"\n");            		
            	}
            }
        }
        
        return lista;
    }

    public List<String> getClassificacaoFinal(String criterioDesempate) {
        List<String> lista= new ArrayList<>();


        lista.add("CLASSIFICACAO FINAL DA PROVA \n");

        for (int i=1; i<=animais.size(); i++) {
            for (Animal a: animais) {
            	if (this.posicoesFinais.get(a) == i) {
            		int tempo = temposFinais.get(a);
                    lista.add(a.getNome() + " " + a.getIdEspecie()+ " "+tempo +"\n");            		
            	}
            }
        }
        return lista;    	
    }
    
    public List<String> getClassificacaoEquipas() {
        List<String> lista= new ArrayList<>();

        lista.add("CLASSIFICACAO POR EQUIPAS \n");
        lista.add("\n");


        for (int i=1; i<=animais.size(); i++) {
            for (Animal a: animais) {
            	if (this.posicoesFinais.get(a) == i) {
            		int tempo = temposFinais.get(a);
                    lista.add(a.getNome() + " " + a.getIdEspecie()+ " "+tempo +"\n");            		
            	}
            }
        }

        return lista;    	
    }
    
    public List<String> getTabelaPremios(){ //List de Strings representando a tabela de prémios
        List<String> lista= new ArrayList<>();


        lista.add("TABELA DE PREMIOS \n");
        lista.add("\n");

        for (Premio a: getPremios())
        {
            lista.add(a.nome + " " + a.valor + " " + "\n\n");

        }

        return lista;
    }

    public void escreverResultados (String filename) throws FileNotFoundException { //Escrever no ficheiro indicado pelo filename a info da classificação geral e tabela de prémios
        System.out.println("entrou");


        try{
            String nomeFicheiroResultados = filename;


            File file = new File(nomeFicheiroResultados);
            FileWriter escreve = new FileWriter(file);






                    escreve.write("Lista de Resultados:\n");
                    escreve.write("\n");

                    for (Animal a: getAnimais()){
                        escreve.write(a.getNome() + "\n");
                    }
                    for(Premio a: getPremios()) {
                        escreve.write(a.nome + "\n");
                    }
                escreve.close();

        }catch (IOException err){

            System.out.println("Erro" + err);

        }



    }


}
