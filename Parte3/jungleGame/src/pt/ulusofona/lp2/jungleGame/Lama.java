package pt.ulusofona.lp2.jungleGame;

public class Lama extends Terreno {

	@Override
	public double getAtrito(boolean muitoLento, boolean voador, boolean nadador,int rochosidade) {
		if (muitoLento) return 0;
		return 1;
	}

}
