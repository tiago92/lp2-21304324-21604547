package pt.ulusofona.lp2.jungleGame;

public class Zebra extends Animal {

	public Zebra(String nome, int id, int velocidadeBase, int energiaInicial, String image,
			int inteligencia) {
		super(nome, id, 9, velocidadeBase, energiaInicial, image, inteligencia);
	}

}
