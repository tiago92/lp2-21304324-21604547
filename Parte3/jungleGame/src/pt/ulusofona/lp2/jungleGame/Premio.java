package pt.ulusofona.lp2.jungleGame;

public class Premio {

    String nome;
    int valor;
    int posicaoPremio;
    boolean acumulavel;

    public Premio(String nome, int valor, int posicaoPremio) {
        this.nome = nome;
        this.valor = valor;
        this.posicaoPremio = posicaoPremio;


    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public int getPosicaoPremio() {
        return posicaoPremio;
    }

    public void setPosicaoPremio(int posicaoPremio) {
        this.posicaoPremio = posicaoPremio;
    }
}
