package pt.ulusofona.lp2.jungleGame;

public class Formiga extends Animal {

	public Formiga(String nome, int id, int velocidadeBase, int energiaInicial, String image,
				   int inteligencia) {
		super(nome, id, 0, velocidadeBase, energiaInicial, image, inteligencia);
	}


	@Override
	public boolean isMuitoLento(){
		return true;
	}

}
