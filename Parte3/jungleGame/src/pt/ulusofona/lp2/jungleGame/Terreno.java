package pt.ulusofona.lp2.jungleGame;

public abstract class Terreno {

	public abstract double getAtrito(boolean muitoLento, boolean voador, boolean nadador, int rochosidade);
}
