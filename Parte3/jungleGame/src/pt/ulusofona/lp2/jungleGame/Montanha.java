package pt.ulusofona.lp2.jungleGame;

public class Montanha extends Terreno {

	@Override
	public double getAtrito(boolean muitoLento, boolean voador, boolean nadador,int rochosidade) {
		if (voador || muitoLento) return 0;

		return 1;
	}

}
