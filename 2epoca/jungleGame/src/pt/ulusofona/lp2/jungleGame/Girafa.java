package pt.ulusofona.lp2.jungleGame;

public class Girafa extends Animal {

	public Girafa(String nome, int id, int velocidadeBase, int energiaInicial, String image,
			int inteligencia) {
		super(nome, id, 1, velocidadeBase, energiaInicial, image, inteligencia);
	}
	

	@Override
	public int GetRochosidade() {
		return 2;
	}

}
