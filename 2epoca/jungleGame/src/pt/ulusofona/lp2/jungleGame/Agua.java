package pt.ulusofona.lp2.jungleGame;

public class Agua extends Terreno {

	@Override
	public double getAtrito(boolean muitoLento, boolean voador, boolean nadador,int rochosidade) {
		if ((voador || nadador) || muitoLento) return 0;
		return 1;
	}

}
