package pt.ulusofona.lp2.jungleGame;

import java.io.*;
import java.util.List;


public class Animal {

	public int getId() {
		return id;
	}



	public int getEnergiaInicial() {
		return energiaInicial;
	}




	public int getInteligencia() {
		return inteligencia;
	}

	private int posicao;
	private String nome;
	private int id;
	private int idEspecie;
	private int velocidadeBase;
	private int energiaInicial;
	private String imagePNG;
	private int inteligencia;

	private int energia;

    int ultimoTurno;

    public Animal(String nome, int id,int idEspecie, int velocidadeBase, int energiaInicial, String image, int inteligencia) {
        this.nome = nome;
        this.id = id;
        this.idEspecie =idEspecie;
        this.velocidadeBase = velocidadeBase;
        this.energiaInicial = energiaInicial;
        this.energia = energiaInicial;
        this.imagePNG = image;

    }




    public int getPosicao (){ //Devolve posição actual do Animal.

        return posicao;
    }

    public void escreveEmFicheiro(int turno, int totalTurnos, String nomeFile){
        try{
            File file = new File(nomeFile);
            if(!file.exists()){
                file.createNewFile();
            }
            FileWriter fileWritter = new FileWriter(file.getName(),true);
            BufferedReader bR = new BufferedReader(new FileReader(nomeFile));
            BufferedWriter bufferWritter = new BufferedWriter(fileWritter);

            if(turno==0)
                bufferWritter.write("CLASSIFICACAO FINAL DA PROVA"+"\r\n");



            bufferWritter.close();
            bR.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public boolean move(int turno, Terreno t, int maxPos, List<Fenonomo> fenonomos){

        boolean muitoLento = this.isMuitoLento();
    	boolean voador = this.IsVoador();
        boolean nadador = this.IsNadador();
    	int rochosidade = this.GetRochosidade();
    	
        int inteligenciaTurno = this.inteligencia;
        int velocidadeTurno = velocidadeBase;
        int energiaTurno = this.energia;
        
        for (Fenonomo f: fenonomos) {
        	
        	if (f.getTurno() == turno)
        	
        	switch (f.getTipo()) {
        	case 1: if (inteligenciaTurno >1) inteligenciaTurno --; break;
        	case 2: energiaTurno *=2; break;
        	case 3: velocidadeTurno++; break;
           	case 4: if (velocidadeTurno>1) velocidadeTurno--; break;
           	case 5: if (velocidadeTurno>4) velocidadeTurno-=2; break;
                   	}
        }
        
    	double atrito = t.getAtrito(muitoLento,voador, nadador, rochosidade);
    	
        int distancia = (int)((velocidadeTurno * energiaTurno * 0.5) + (1 - atrito)); // cast de double para int

        if(verificaEnergia() && distancia>0){
        	
        	int energiaGasta = (int)(distancia - inteligenciaTurno * 0.5);
        	
        	if (energiaGasta<1) energiaGasta = 1;
        	
                energia -= energiaGasta;
                
                this.posicao += distancia;
                if (this.posicao>maxPos) this.posicao = maxPos;
        }else{
        	int energiaRecuperada = (int) Math.max(Math.min(inteligenciaTurno*0.5, this.energiaInicial), 1);
            energia+=energiaRecuperada;
        }

        if(posicao==11)
            return false;

        ultimoTurno++;

        return true;

    }

    public boolean IsVoador() {
		return false;
	}
	public boolean isMuitoLento(){
        return false;
    }
    public boolean IsNadador() {
		return false;
	}

    public int GetRochosidade() {
		return 1;
	}



	public boolean verificaEnergia(){
        if(energia>0)
            return true;
        else
            return false;
    }

    public String getNome() {
        return nome;
    }

    public int getUltimoTurno() {
        return ultimoTurno;
    }




	public int getIdEspecie() {
		return this.idEspecie;
	}

	public String getImagePNG() {
		return this.imagePNG;
	}
}



    
    

