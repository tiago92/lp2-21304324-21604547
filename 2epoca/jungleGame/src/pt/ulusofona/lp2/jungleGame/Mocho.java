package pt.ulusofona.lp2.jungleGame;

public class Mocho extends Animal {

	public Mocho(String nome, int id, int velocidadeBase, int energiaInicial, String image,
			int inteligencia) {
		super(nome, id, 6, velocidadeBase, energiaInicial, image, inteligencia);

	}
	
	@Override
    public boolean IsVoador() {
		return true;
	}

}
