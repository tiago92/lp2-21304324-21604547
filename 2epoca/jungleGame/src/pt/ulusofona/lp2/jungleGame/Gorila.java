package pt.ulusofona.lp2.jungleGame;

public class Gorila extends Animal {

	public Gorila(String nome, int id, int velocidadeBase, int energiaInicial, String image,
			int inteligencia) {
		super(nome, id, 2, velocidadeBase, energiaInicial, image, inteligencia);
	}

	@Override
	public int GetRochosidade() {
		return 0;
	}

}
