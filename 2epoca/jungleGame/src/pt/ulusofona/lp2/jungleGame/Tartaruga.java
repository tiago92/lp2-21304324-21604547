package pt.ulusofona.lp2.jungleGame;

public class Tartaruga extends Animal {

	public Tartaruga(String nome, int id, int velocidadeBase, int energiaInicial, String image,
			int inteligencia) {
		super(nome, id, 7, velocidadeBase, energiaInicial, image, inteligencia);
	}

	@Override	
    public boolean IsNadador() {
		return true;
	}

}
