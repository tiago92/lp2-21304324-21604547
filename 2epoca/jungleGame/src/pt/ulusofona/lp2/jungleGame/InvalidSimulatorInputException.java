package pt.ulusofona.lp2.jungleGame;

public class InvalidSimulatorInputException extends Exception {

	private String msg;
	private int line;
	
	public InvalidSimulatorInputException(String mensagem, int line) {
		this.msg = mensagem;
		this.line = line;
	}

	public int getInputLine() {
			return this.line;
	}
}
