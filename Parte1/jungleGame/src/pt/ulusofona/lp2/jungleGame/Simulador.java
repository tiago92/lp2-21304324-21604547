package pt.ulusofona.lp2.jungleGame;

import java.io.*;
import java.util.*;


public class Simulador {

    List <Animal> animais = new ArrayList<Animal>();
    List <Premio> premios = new ArrayList<Premio>();
    List <Animal> posicoesFinais= new ArrayList<>();

    int nPersonagens;
    int nPremios;

    public boolean iniciaJogo (File ficheiroInicial, int comprimentoPista){ //Faz a leitura do ficheiro texto e carrega para a memória os animais e prémios presentes na simulação

        int nLinha = 0;

        String[] dados;

        try {
            ficheiroInicial = new File("input.txt");
            Scanner leitorFicheiro = new Scanner(ficheiroInicial);

            while (leitorFicheiro.hasNextLine()) {
                if(nLinha==0) {
                    String linha1 = leitorFicheiro.nextLine();
                    dados = linha1.split(":");
                    this.nPersonagens= Integer.parseInt(dados[0]);
                    this.nPremios = Integer.parseInt(dados[1]);
                    System.out.println(nPersonagens+" "+nPremios);
                }if (nLinha>0 && nLinha<=nPersonagens){
                    String linha1 = leitorFicheiro.nextLine();
                    dados = linha1.split(":");
                    String nome = dados[0];
                    int id = Integer.parseInt(dados[1]);
                    int idEspecie = Integer.parseInt(dados[2]);
                    int velocidadeBase = Integer.parseInt(dados[3]);
                    int energiaInicial = Integer.parseInt(dados[4]);
                    if(idEspecie==3){
                        animais.add(new Animal(nome,id,idEspecie,velocidadeBase,energiaInicial, "tarzan.png"));
                    }if(idEspecie==4){
                        animais.add(new Animal(nome,id,idEspecie,velocidadeBase,energiaInicial, "simba.jpg"));
                    }if(idEspecie==6){
                        animais.add(new Animal(nome,id,idEspecie,velocidadeBase,energiaInicial, "coruja.jpg"));
                    }
                }if (nLinha>nPersonagens && nLinha<=(nPremios+nPersonagens)){
                    String linha1 = leitorFicheiro.nextLine();
                    dados = linha1.split("\\s*:\\s*");
                    String nomePremio = dados[0];
                    int valor = Integer.parseInt(dados[1]);
                    int posicao = Integer.parseInt(dados[2]);

                    premios.add(new Premio(nomePremio,valor,posicao));
                }


                nLinha++;
            }
            leitorFicheiro.close();
        }catch (FileNotFoundException exception) {
            String mensagem = "Erro nao foi possivel abrir input.txt";
            System.out.println(mensagem);
        }

        return true;
    }

    public boolean processaTurno(){  //Executa o turno/tempo da simulação
        int animaisNaUltimaPos=0;
        boolean existe=false;
        for (Animal animal : animais){ //Percorre os animais e faz a sua distância
         boolean a = animal.move();
          if(!a){
              animaisNaUltimaPos++;
              for(Animal o: posicoesFinais)
                  if(o.equals(animal))
                      existe=true;
              if(!existe)
                posicoesFinais.add(animal);
          }
         /*else{

              for(Animal o: posicoesFinais)
                  if(o.equals(animal))
                      existe=true;
              if(!existe)
                  posicoesFinais.add(animal);
          }*/
        }
        for(Animal a:posicoesFinais){
            System.out.println(a.getNome());
        }

        if (animaisNaUltimaPos>2)
            return false;
        return true;
    }


    public List<Premio> getPremios() {
        return premios;
    }

    public List<Premio> getListaPremioPorPosicao(int c, List<Premio> premios)  {
        List<Premio> temp = new ArrayList<>();
        for (Premio p1: premios) {
            if (p1.posicaoPremio == c){
                temp.add(p1);
            }
        }
        return temp;
    }


    public List <Animal> getAnimais (){ //Lista de todos os animais presentes na simulção

        return animais;
    }

    public List<String> getClassificacaoGeral(){ //Lista de Strings quem representa a classificação geral da prova
       List<String> lista= new ArrayList<>();


       lista.add("CLASSIFICACAO FINAL DA PROVA \n");
       lista.add("\n");

       for (Animal a: posicoesFinais) {
            lista.add(a.nome + " " + a.idEspecie+ " " +"\n");
        }

       return lista;
    }

    public List<String> getTabelaPremios(){ //List de Strings representando a tabela de prémios
        List<String> lista= new ArrayList<>();


        lista.add("TABELA DE PREMIOS \n");
        lista.add("\n");

        for (Premio a: getPremios())
        {
            lista.add(a.nome + " " + a.valor + " " + "\n\n");

        }

        return lista;
    }

    public void escreverResultados (String filename) throws FileNotFoundException { //Escrever no ficheiro indicado pelo filename a info da classificação geral e tabela de prémios
        System.out.println("entrou");


        try{
            String nomeFicheiroResultados = filename;


            File file = new File(nomeFicheiroResultados);
            FileWriter escreve = new FileWriter(file);






                    escreve.write("Lista de Resultados:\n");
                    escreve.write("\n");

                    for (Animal a: getAnimais()){
                        escreve.write(a.nome + "\n");
                    }
                    for(Premio a: getPremios()) {
                        escreve.write(a.nome + "\n");
                    }
                escreve.close();

        }catch (IOException err){

            System.out.println("Erro" + err);

        }



    }

}